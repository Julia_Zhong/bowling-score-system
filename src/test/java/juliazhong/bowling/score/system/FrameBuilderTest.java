package juliazhong.bowling.score.system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.rules.ExpectedException;

import juliazhong.bowling.score.system.ApplicationException;
import juliazhong.bowling.score.system.Frame;
import juliazhong.bowling.score.system.FrameBuilder;
import juliazhong.bowling.score.system.Bowl;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class FrameBuilderTest {

	private FrameBuilder builder;

	@Before
	public void setUp() {
		builder = new FrameBuilder();
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void can_buildFrame() {
		ArrayList<Bowl> rolls = new ArrayList<>();
		rolls.add(new Bowl(5));
		rolls.add(new Bowl(3));
		ArrayList<Frame> frames = builder.build(rolls);
		assertNotEquals(0, frames.get(0).firstRollPinsDown());
		assertEquals(8, frames.get(0).totalPins());
	}

	public void can_buildStrikeFrame() {
		ArrayList<Bowl> rolls = new ArrayList<>();
		rolls.add(new Bowl(10));
		ArrayList<Frame> frames = builder.build(rolls);
		assertNotEquals(0, frames.get(0).firstRollPinsDown());
		assertEquals(10, frames.get(0).totalPins());
	}

	public void can_buildSpareFrame() {
		ArrayList<Bowl> rolls = new ArrayList<>();
		rolls.add(new Bowl(3));
		rolls.add(new Bowl(5));
		rolls.add(new Bowl(7));
		rolls.add(new Bowl(3));
		ArrayList<Frame> frames = builder.build(rolls);
		assertNotEquals(0, frames.get(0).firstRollPinsDown());
		assertEquals(10, frames.get(1).totalPins());
	}

	@Test
	public void throw_exception_ifSumOfPinsMoreThanTen() {
		thrown.expect(ApplicationException.class);
		thrown.expectMessage("Invalid number of pins");
		ArrayList<Bowl> rolls = new ArrayList<>();
		rolls.add(new Bowl(9));
		rolls.add(new Bowl(2));
		builder.build(rolls);
	}
}
