package juliazhong.bowling.score.system;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import juliazhong.bowling.score.system.ApplicationException;
import juliazhong.bowling.score.system.BowlingGame;
import juliazhong.bowling.score.system.Player;
import juliazhong.bowling.score.system.ScoreSystem;

public class ScoreSystemTest {

	private ScoreSystem scoreSystem;
	private BowlingGame game;

	@Before
	public void setUp() {
		scoreSystem = new ScoreSystem();
		game = new BowlingGame();
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	
	@Test
	public void scoreStrike() {
		Player player = new Player("player");
		bowl(player, 10, 3, 5);
		assertThat(scoreSystem.score(player), is(26));
	}

	@Test
	public void scoreSpare() {
		Player player = new Player("player");
		bowl(player, 7, 3, 5);
		assertThat(scoreSystem.score(player), is(20));
	}

	@Test
	public void scoreAllStrikes() {
		Player player = new Player("player");
		bowl(player, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);
		assertThat(scoreSystem.score(player), is(300));
	}

	@Test
	public void scoreAllZero() {
		Player player = new Player("player");
		bowl(player, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		assertThat(scoreSystem.score(player), is(0));
	}

	@Test
	public void scoreWholeGame() {
		Player player = new Player("player");
		bowl(player, 10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 3, 3);
		assertThat(scoreSystem.score(player), is(168));
	}
	
	@Test
	public void scoreAllSpare() {
		Player player = new Player("player");
		bowl(player, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5);
		assertThat(scoreSystem.score(player), is(150));
	}
	
	@Test
	public void canChangePlayer() {
		Player player = new Player("player");
		Player secondPlayer = new Player("SecondPlayer");
		bowl(player, 5, 1);
		bowl(secondPlayer, 2, 7);
		assertThat(scoreSystem.score(player), is(6));
		assertThat(scoreSystem.score(secondPlayer), is(9));
	}

	@Test
	public void throw_exception() {
		thrown.expect(ApplicationException.class);
		thrown.expectMessage("Invalid number of pins");
		Player player = new Player("player");
		bowl(player, 11);
	}

	private void bowl(Player player, int... pins) {
		for (int i = 0; i < pins.length; i++) {
			game.bowl(player, pins[i]);
		}
	}
}
