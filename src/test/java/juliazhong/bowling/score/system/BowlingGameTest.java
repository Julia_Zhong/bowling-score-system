package juliazhong.bowling.score.system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import juliazhong.bowling.score.system.BowlingGame;
import juliazhong.bowling.score.system.Player;

public class BowlingGameTest {

	private BowlingGame game;
	
	@Before
	public void setUp() {
		game = new BowlingGame();
	}
	
	@Test
	public void getScoreBoardWholeGame() {
		Player player = new Player("player");
		bowl(player, 10, 7, 3, 7, 2, 9, 1, 10, 10, 10, 2, 3, 6, 4, 7, 3, 3);
		Map<Integer, Integer> scoreBoard = game.getFrameScoreBoard(player);
		assertNotNull(scoreBoard);
		int value1 = scoreBoard.get(1);
		int value2 = scoreBoard.get(2);
		int value3 = scoreBoard.get(3);
		int value4 = scoreBoard.get(4);
		int value5 = scoreBoard.get(5);
		int value6 = scoreBoard.get(6);
		int value7 = scoreBoard.get(7);
		int value8 = scoreBoard.get(8);
		int value9 = scoreBoard.get(9);
		int value10 = scoreBoard.get(10);

		assertEquals(20, value1);
		assertEquals(17, value2);
		assertEquals(9, value3);
		assertEquals(20, value4);
		assertEquals(30, value5);
		assertEquals(22, value6);
		assertEquals(15, value7);
		assertEquals(5, value8);
		assertEquals(17, value9);
		assertEquals(13, value10);
	}

	

	private void bowl(Player player, int... pins) {
		for (int i = 0; i < pins.length; i++) {
			game.bowl(player, pins[i]);
		}
	}
}
