package juliazhong.bowling.score.system;

import java.util.ArrayList;
import java.util.List;

public class Player {

	private String name;
	private ArrayList<Frame> frames;

	public Player(String playerName) {
		this.name = playerName;
		this.frames = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public List<Frame> getFrames() {
		return frames;
	}

	public void setFrames(ArrayList<Frame> frames) {
		this.frames = frames;
	}

}
