package juliazhong.bowling.score.system;

class Frame {
	private int frameNumber;
	private Bowl firstBowl;
	private Bowl secondBowl;
	private int frameScore;

	public Frame(Integer frameNumber, Bowl bowl) {
		this(frameNumber, bowl, Bowl.miss());
	}

	public Frame(Integer frameNumber, Bowl firstBowl, Bowl secondBowl) {
		this.firstBowl = firstBowl;
		this.secondBowl = secondBowl;
		this.frameNumber = frameNumber;
	}

	public Frame(Integer frameNumber, Bowl firstRoll, Bowl secondRoll, int frameScore) {
		this.firstBowl = firstRoll;
		this.secondBowl = secondRoll;
		this.frameNumber = frameNumber;
		this.frameScore = frameScore;
	}

	public int totalPins() {
		return firstBowl.pinsDown() + secondBowl.pinsDown();
	}

	public int firstRollPinsDown() {
		return firstBowl.pinsDown();
	}

	public boolean isStrike() {
		return false;
	}

	public boolean isSpare() {
		return false;
	}

	public int getFrameNumber() {
		return frameNumber;
	}

	public void scoreFrame(int frameScore) {
		this.frameScore = frameScore;
	}

	public Integer getFrameScore() {
		return this.frameScore;
	}

}
