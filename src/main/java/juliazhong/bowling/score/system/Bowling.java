package juliazhong.bowling.score.system;

import java.util.Map;

public interface Bowling {

	/**
	 * <p>
	 * Adds value of throw to the player's list of bowls.
	 * </p>
	 * 
	 * @param player
	 *            the player who throw a ball
	 * @param pins
	 *            the amount of pins knocked down by one bowl
	 */
	public void bowl(Player player, int pins);

	/**
	 * <p>
	 * Calculate total score of all bowls that have been thrown by player at the
	 * moment.
	 * </p>
	 * 
	 * @param player
	 *            the player who's score is calculated
	 */
	public int scoreOf(Player player);

	/**
	 * <p>
	 * Gets the mapping of frame number and it's score for player;
	 * </p>
	 * 
	 * @param player
	 * 
	 */
	public Map<Integer, Integer> getFrameScoreBoard(Player player);

}
