package juliazhong.bowling.score.system;

import java.util.ArrayList;

class FrameBuilder {

	public ArrayList<Frame> build(ArrayList<Bowl> bowls) {
		ArrayList<Frame> frames = frames();
		int numberOfBowl = 0;
		int numberOfFrame = 0;
		while (numberOfBowl < bowls.size()) {
			Frame frame = getTurn(numberOfFrame, numberOfBowl, bowls);
			frames.set(numberOfFrame, frame);
			numberOfFrame++;
			numberOfBowl += frame.isStrike() ? 1 : 2;
		}
		return frames;
	}

	private ArrayList<Frame> frames() {
		ArrayList<Frame> frames = new ArrayList<>();
		for (int i = 0; i < 12; i++) {
			frames.add(i, new Frame(i, Bowl.miss(), Bowl.miss(), 0));
		}
		return frames;
	}

	private Frame getTurn(Integer numberOfFrame, int startingBowl, ArrayList<Bowl> bowls) {
		Bowl firstBowl = getBowl(startingBowl, bowls);
		if (firstBowl.pinsDown() == 10) {
			return new Strike(numberOfFrame, firstBowl);
		}
		Bowl secondBowl = getBowl(startingBowl + 1, bowls);
		if (firstBowl.pinsDown() + secondBowl.pinsDown() > 10) {
			throw new ApplicationException("Invalid number of pins");
		}
		if (firstBowl.pinsDown() + secondBowl.pinsDown() == 10) {
			return new Spare(numberOfFrame, firstBowl, secondBowl);
		}
		return new Frame(numberOfFrame, firstBowl, secondBowl);
	}

	private Bowl getBowl(int bowlNumber, ArrayList<Bowl> bowls) {
		return bowlNumber < bowls.size() ? bowls.get(bowlNumber) : Bowl.miss();
	}

}
