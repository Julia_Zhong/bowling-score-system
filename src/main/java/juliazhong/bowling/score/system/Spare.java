package juliazhong.bowling.score.system;

class Spare extends Frame {
	
	public Spare(Integer frameNumber, Bowl firstBowl, Bowl secondBowl) {
		super(frameNumber, firstBowl, secondBowl);
	}

	@Override
	public boolean isSpare() {
		return true;
	}
}
