package juliazhong.bowling.score.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class BowlingGame implements Bowling {
	private Map<Player, ArrayList<Bowl>> gameThrows;
	private ScoreSystem scoreSystem;
	private FrameBuilder builder;

	public BowlingGame() {
		this.builder = new FrameBuilder();
		this.gameThrows = new HashMap<>();
		this.scoreSystem = new ScoreSystem();
	}

	public void bowl(Player player, int pins) {
		if (!gameThrows.containsKey(player)) {
			addNewPlayer(player);
		}
		if (pins > 10) {
			throw new ApplicationException("Invalid number of pins");
		}
		ArrayList<Bowl> bowls = gameThrows.get(player);
		bowls.add(bowls.size(), new Bowl(pins));
		player.setFrames(builder.build(bowls));
	}

	@Override
	public int scoreOf(Player player) {
		return scoreSystem.score(player);
	}

	@Override
	public Map<Integer, Integer> getFrameScoreBoard(Player player) {
		Map<Integer, Integer> scoreBoard = new HashMap<>();
		scoreOf(player);
		Collection<Frame> frames = player.getFrames().subList(0, 10);
		for (Frame frame : frames) {
			scoreBoard.put(frame.getFrameNumber() + 1, frame.getFrameScore());
		}
		return scoreBoard;
	}

	private void addNewPlayer(Player player) {
		gameThrows.put(player, new ArrayList<Bowl>());
	}
}
