package juliazhong.bowling.score.system;

class ScoreSystem {

	public int score(Player player) {
		int score = 0;
		for (Frame frame : player.getFrames().subList(0, 10)) {
				int frameScore = 0;
				if (frame.isStrike()) {
					frameScore = 10 + strikeBonus(player, frame);
					frame.scoreFrame(frameScore);
					score += frameScore;
				} else if (frame.isSpare()) {
					frameScore = 10 + spareBonus(player, frame);
					frame.scoreFrame(frameScore);
					score += frameScore;
				} else {
					frameScore = frame.totalPins();
					frame.scoreFrame(frameScore);
					score += frameScore;
				}
		}
		return score;

	}

	private int spareBonus(Player player, Frame frame) {
		Frame nextFrame = getNextFrame(player, frame);
		return nextFrame.firstRollPinsDown();
	}

	private int strikeBonus(Player player, Frame frame) {
		Frame nextFrame = getNextFrame(player, frame);
		if (nextFrame.isStrike()) {
			return nextFrame.firstRollPinsDown() + getNextFrame(player, nextFrame).firstRollPinsDown();
		}
		return nextFrame.totalPins();
	}

	private Frame getNextFrame(Player player, Frame frame) {
		int numberOfTurn = frame.getFrameNumber();
		return player.getFrames().get(numberOfTurn + 1);

	}

}
