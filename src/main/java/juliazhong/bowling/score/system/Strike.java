package juliazhong.bowling.score.system;

class Strike extends Frame {

	public Strike(Integer frameNumber, Bowl bowl) {
		super(frameNumber, bowl);
	}

	@Override
	public boolean isStrike() {
		return true;
	}
}
