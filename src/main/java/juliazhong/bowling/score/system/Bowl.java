package juliazhong.bowling.score.system;

class Bowl {
	private int pins;

	public static Bowl miss() {
		return new Bowl(0);
	}

	public Bowl(int pins) {
		this.pins = pins;
	}

	public int pinsDown() {
		return pins;
	}

}
