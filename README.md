A practice solution for Bowling score calculator.

MY SOLUTION
The API consists of three methods:
- bowl() - adds number of pins knocked down for each player. If pins number is more than number allowed by game rules (10) an ApplicationException 				is thrown. If simple total of pins knocked down in one frame is more than ten an ApplicationException is thrown.
- scoreOf() - calculate the score of all knocked down pins at the moment. 
				If player score strike or spare and the next roll(s) needed to calculate frame score haven't been thrown yet, the system adds ten to the frame score first and updates it after each bonus throw.
-getFrameScoreBoard() - returns score for aech frame separetly. Maps frame number with frame score.

